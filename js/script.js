// Завдання №1

let showEyes = document.querySelectorAll('.fas');
let input = document.querySelectorAll('input');
let btn = document.querySelector('.btn');

showEyes.forEach(eye => {
    eye.addEventListener('click', () => {
        if(eye.classList.contains('fa-eye-slash')) {
            eye.classList.remove('fa-eye-slash');
            eye.classList.add('fa-eye');
            input.forEach(el => {
                el.type = 'text';
            })
        } else {
            eye.classList.remove('fa-eye');
            eye.classList.add('fa-eye-slash');
            input.forEach(el => {
                el.type = 'password';
            });
        }
    });
});

btn.addEventListener('click', ()=> {
    let error = document.querySelector('.text-error');
    if (input[0].value == '' && input[1].value == '') {
        error.innerHTML = 'Введіть пароль для вводу';
    } else if (input[0].value === input[1].value) {
        alert('You are welcome');
        error.getElementsByClassName.display = 'none';
        input.forEach(el => el.value = '');
    } else {
        error.innerHTML = 'Потрібно ввести однакові значення';
        error.getElementsByClassName.display = '';
    }
});

